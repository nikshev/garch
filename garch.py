import numpy as np
from scipy import optimize
import statistics as st
import Quandl
import math
from sklearn import linear_model
import csv

# GARCH(1,1) Model in Python
#   uses maximum likelihood method to estimate (omega,alpha,beta)
# (c) 2014 QuantAtRisk, by Pawel Lachowicz; tested with Python 3.5 only
# http://www.quantatrisk.com/2014/10/23/garch11-model-in-python/
# function GARCH11_logL for estimate  parametres
def GARCH11_logL(param, r):
    omega, alpha, beta = param
    omega=abs(omega)
    alpha=abs(alpha)
    beta=abs(beta)
    param_sum=round(omega+alpha+beta,2)
    logL=0
    if (float(param_sum)<float(0.99)) and (float(beta)>float(alpha)) and (float(alpha)>float(0.00001)):
        n = len(r)
        s = np.ones(n)*0.01
        s[2] = st.variance(r[0:3])
        for i in range(3, n):
            s[i] = omega + alpha*r[i-1]**2 + beta*(s[i-1])  # GARCH(1,1) model
        logL = -((-np.log(s) - r**2/s).sum())
    # print ("logL=%.6f\n"%logL)
    # print("omega = %.6f\nbeta  = %.6f\nalpha = %.6f\n" % (omega, beta, alpha))
    return -1*logL

# GARCH(1,1) Model in Python
#   uses maximum likelihood method to estimate (omega,alpha,beta)
# (c) 2014 QuantAtRisk, by Pawel Lachowicz; tested with Python 3.5 only
# http://www.quantatrisk.com/2014/10/23/garch11-model-in-python/
# function GARCH11 for calculate model
def GARCH11(param, r):
    omega, alpha, beta = param
    n = len(r)
    s = np.ones(n)*0.01
    s[2] = st.variance(r[0:3])

    for i in range(3, n):
        s[i] = omega + alpha*r[i-1]**2 + beta*(s[i-1])  # GARCH(1,1) model
    return s


#Get data from Quandl
mydata = Quandl.get("GOOG/NYSE_SPY", trim_start="2008-01-01", trim_end="2015-12-31", authtoken="-sA_RrikArwcyQghGd3j")
r = []
label = []
label_val=0
for i in range(len(mydata.index)):
    value = mydata.iloc[i]["Open"]
    label_val=label_val+1
    if not (math.isnan(value) or math.isinf(value)):
        r.append(value)
        label.append(label_val)
r = np.array(r)
label = np.array(label)

#Get GARCH parameters fmin
o = optimize.fmin(GARCH11_logL, np.array([.1,.1,.1]), args=(r,), full_output=1)
R = np.abs(o[0])
print("fmin")
print("omega = %.15f\nbeta  = %.15f\nalpha = %.55f\n" % (R[0], R[2], R[1]))

#Calculate it
print()
print("GARCH:")
garch= GARCH11(np.array([R[0],R[1],R[2]]),r)

#Write series
with open("/home/eugene/garch.csv", "w") as toWrite:
    writer = csv.writer(toWrite, delimiter=",")
    new_series=[]
    #Create header
    for i in range(10):
        new_series.append("c"+str(i))
    writer.writerow(new_series)

    for i in range (len(garch)):
        new_series=[]
        for j in range(10):
            if (i+j)<len(garch):
                new_series.append(garch[i+j])

        if (len(new_series)==10):
            writer.writerow(new_series)




